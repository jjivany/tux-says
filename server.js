const express = require('express');
const app = express();
const { exec } = require('child_process');

app.use(express.static('static'));

app.get('/status', (req, res) => {
  exec('uname -a', (err, stdout, stderr) => {
    res.send(stdout);
  });
});

app.listen(3000, () => console.log('Listening on port 3000!!!!'));
